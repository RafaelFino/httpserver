package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"
)

func main() {
	http.HandleFunc("/", handle)
	http.HandleFunc("/echo", handleEcho)

	log.Fatal(http.ListenAndServe(os.Args[1], nil))
}

func handle(w http.ResponseWriter, r *http.Request) {
    log.Println("request http")
    fmt.Fprintln(w, readFile())
}

type requestPar struct {
	When   time.Time
	Key    string
	Values []string
}

func readFile() string {
	dat, err := ioutil.ReadFile("index.html")
	if err != nil {
		panic(err)
	}
	return string(dat)
}

func handleEcho(w http.ResponseWriter, r *http.Request) {
	data := make([]requestPar, 0)
	for key, value := range r.URL.Query() {
		data = append(data, requestPar{When: time.Now(), Key: key, Values: value})
	}
	raw, err := json.MarshalIndent(data, "", "\t")
	if err != nil {
		fmt.Fprintln(w, fmt.Sprintf("<html><body><b>Echo error:</b><br>%s</body></html>", err))
	} else {
		fmt.Fprintln(w, string(raw))
	}
}
